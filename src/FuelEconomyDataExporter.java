import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.IllegalFormatException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.csv.CSVFormat;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(urlPatterns = {"/FuelEconomyDataExporter"}, loadOnStartup=1)
@MultipartConfig
public class FuelEconomyDataExporter extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    private final static Logger LOGGER = Logger.getLogger(FuelEconomyDataExporter.class.getCanonicalName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = null;
        Connection conn = null;
        
        System.out.println("test3");

        try {
        	String selectAllQuery = "SELECT [id]\n" +
                    "        \t\t      ,[year]\n" +
                    "        \t\t      ,[make]\n" +
                    "        \t\t      ,[model]\n" +
                    "        \t\t      ,[trany]\n" +
                    "        \t\t      ,[cylinders]\n" +
                    "        \t\t      ,[displ]\n" +
                    "        \t\t      ,[tCharger]\n" +
                    "        \t\t      ,[sCharger]\n" +
                    "        \t\t      ,[city08]\n" +
                    "        \t\t      ,[highway08]\n" +
                    "        \t\t      ,[comb08]\n" +
                    "        \t\t      ,[co2TailpipeGpm]\n" +
                    "        \t\t      ,[ghgScore]\n" +
                    "        \t\t      ,[feScore]\n" +
                    "        \t\t      ,[fuelCost08]\n" +
                    "        \t\t  FROM [CharlotteFleetEmmissions].[dbo].[vehicles]";
        	
            conn = Utils.connectToSqlServer();
            Statement statement = conn.createStatement();
            ResultSet resultSet = statement.executeQuery(selectAllQuery);
            
            writer = new PrintWriter(response.getOutputStream());
            response.setHeader("Content-Disposition", "attachment; filename=\"vehicles.csv\"");
            
            // Print ResultSet to CSV
        	CSVFormat.DEFAULT.withHeader(resultSet).print(writer).printRecords(resultSet);
            writer.flush();
            
            LOGGER.log(Level.INFO, "Data exported",
                    new Object[]{});

        } catch (FileNotFoundException e) {
            LOGGER.log(Level.SEVERE, "{0}", new Object[]{e.getMessage()});
        } catch (IOException | IllegalFormatException e) {
        	LOGGER.log(Level.SEVERE, "{0}", new Object[]{e.getMessage()});
        } catch (SQLException e) {
        	LOGGER.log(Level.SEVERE, "{0}", new Object[]{e.getMessage()});
        } finally {
            if (writer != null) {
            	writer.close();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
        
        System.out.println("test");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
        System.out.println("test2");
    }
	
}
