import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Utils {
    static Connection connectToSqlServer() throws SQLException {
        String url = "jdbc:sqlserver://";
        String server = "localhost";
        int port = 62463;
        String dbName = "CharlotteFleetEmmissions";
        String username = "sa";
        String password = "admin";

        String driverClass = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

        String connUrl = String.format(
                "%s%s:%d;databaseName=%s",
                url, server, port, dbName);

        // Make sure driver exists
        try {
            Class.forName(driverClass);
        } catch (java.lang.ClassNotFoundException e) {
            System.err.println("No driver class found for: " + driverClass);
            e.printStackTrace();
        }

        Connection conn = DriverManager.getConnection(connUrl, username, password);
        return conn;
    }
}
