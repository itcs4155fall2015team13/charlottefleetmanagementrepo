
import java.sql.ResultSet;

public class DirectQueryTester {

	public void initialize(){
		/**
		 * This method just tests a series of parameter based 
		 * simple queries provided by the DatabaseController
		 */
		DatabaseController dbc = DatabaseController.getDBController();
		
		//Test the getCustomerByID() method
		System.out.println("Testing...");
		System.out.println(dbc.getTableNames());
		dbc.printResultSet(dbc.executeQuery("SELECT * FROM vehicle_emmission_2016"));
		System.out.println("");
		
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		DirectQueryTester dqt = new DirectQueryTester();
		dqt.initialize();

	}

}