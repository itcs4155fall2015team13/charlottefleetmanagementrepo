import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.IllegalFormatException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@WebServlet(urlPatterns = {"/uploadCsv"}, loadOnStartup=1)
@MultipartConfig
public class FuelEconomyDataImporter extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    private final static Logger LOGGER = Logger.getLogger(FuelEconomyDataImporter.class.getCanonicalName());

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Create path components to save the file
        final Part filePart = request.getPart("file");
        final String fileName = getFileName(filePart);

        OutputStream out = null;
        InputStream filecontent = null;
        Connection conn = null;

        try {
        	final File tempFile = File.createTempFile("vehicles", "_"+Long.toString(System.nanoTime())+".csv");
            out = new FileOutputStream(tempFile);
            filecontent = filePart.getInputStream();

            int read;
            final byte[] bytes = new byte[1024];

            while ((read = filecontent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            
            LOGGER.log(Level.INFO, "File {0} being uploaded to {1}",
                    new Object[]{fileName, tempFile.getAbsolutePath()});
            
            // Connect to database
            conn = Utils.connectToSqlServer();

            // Upload workbook data to DB
            uploadToSqlServer(tempFile, conn);
                
            LOGGER.log(Level.INFO, "Data imported",
                    new Object[]{});

        } catch (FileNotFoundException e) {
            LOGGER.log(Level.SEVERE, "{0}", new Object[]{e.getMessage()});
        } catch (IOException | IllegalFormatException e) {
        	LOGGER.log(Level.SEVERE, "{0}", new Object[]{e.getMessage()});
        } catch (SQLException e) {
        	LOGGER.log(Level.SEVERE, "{0}", new Object[]{e.getMessage()});
        } finally {
            if (out != null) {
                out.close();
            }
            if (filecontent != null) {
                filecontent.close();
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            response.sendRedirect("Admin.jsp");
        }
    }

    private String getFileName(final Part part) {
        final String partHeader = part.getHeader("content-disposition");
        LOGGER.log(Level.INFO, "Part Header = {0}", partHeader);
        for (String content : part.getHeader("content-disposition").split(";")) {
            if (content.trim().startsWith("filename")) {
                return content.substring(
                        content.indexOf('=') + 1).trim().replace("\"", "");
            }
        }
        return null;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }
	
    /** Upload fuel economy CSV to SQL Server */
    static void uploadToSqlServer(File csvFile, Connection conn) throws SQLException, IOException {
        Statement statement = conn.createStatement();

//          // Create SQL table
//        statement.executeUpdate("DROP TABLE IF EXISTS FuelEconomy");
//        StringBuilder sb = new StringBuilder("CREATE TABLE FuelEconomy (");
//        sb.append("[id] [float] NULL").append(",");
//        sb.append("[year] [float] NULL").append(",");
//        sb.append("[make] [nvarchar](255) NULL").append(",");
//        sb.append("[model] [nvarchar](255) NULL").append(",");
//        sb.append("[trany] [nvarchar](255) NULL").append(",");
//        sb.append("[cylinders] [float] NULL").append(",");
//        sb.append("[displ] [float] NULL").append(",");
//        sb.append("[tCharger] [nvarchar](255) NULL").append(",");
//        sb.append("[sCharger] [nvarchar](255) NULL").append(",");
//        sb.append("[city08] [float] NULL").append(",");
//        sb.append("[highway08] [float] NULL").append(",");
//        sb.append("[comb08] [float] NULL").append(",");
//        sb.append("[co2TailpipeGpm] [float] NULL").append(",");
//        sb.append("[ghgScore] [float] NULL").append(",");
//        sb.append("[feScore] [float] NULL").append(",");
//        sb.append("[fuelCost08] [float] NULL");
//        sb.append(")");
//        statement.executeUpdate(sb.toString());

        // Apache Commons CSV Parser
        CSVParser parser = CSVFormat.EXCEL.withHeader()
                .parse(new FileReader(csvFile));

        int i = 0;
        for (CSVRecord record : parser) {
            if (++i % 1000 == 0) System.out.println("Row " + i);

            StatementBuilder stb = new StatementBuilder("INSERT INTO vehicles VALUES (");
            stb.appendNumValue(record.get("id"));
            stb.appendValue(Double.parseDouble(record.get("year")));
            stb.appendValue(record.get("make"));
            stb.appendValue(record.get("model"));
            stb.appendValue(record.get("trany"));
            stb.appendNumValue(record.get("cylinders"));
            stb.appendNumValue(record.get("displ"));
            stb.appendValue(record.get("tCharger"));
            stb.appendValue(record.get("sCharger"));
            stb.appendNumValue(record.get("city08"));
            stb.appendNumValue(record.get("highway08"));
            stb.appendNumValue(record.get("comb08"));
            stb.appendNumValue(record.get("co2TailpipeGpm"));
            stb.appendNumValue(record.get("ghgScore"));
            stb.appendNumValue(record.get("feScore"));
            stb.appendNumValue((record.get("fuelCost08")));
            stb.append(")");
            statement.executeUpdate(stb.toString());
        }
    }

}
