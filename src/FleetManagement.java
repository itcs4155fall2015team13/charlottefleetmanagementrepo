import java.io.IOException;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class FleetManagement
 */
@WebServlet(urlPatterns = {"/FleetManagement"}, loadOnStartup=1)
public class FleetManagement extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//System.out.println(request);
		//System.out.println(response);
		//System.out.println(request.getParameter("pageRedirect"));
		//System.out.println(request.getParameter("oper"));
		
		if(request.getParameter("oper").compareTo("pageRedirect") == 0){
			if(request.getParameter("pageRedirect").compareTo("login") == 0){
				//response.sendRedirect(request.getContextPath() + "/Dashboard.jsp");
				//request.setAttribute("page", request.getContextPath() + "/Dashboard.jsp");
				response.getWriter().write(request.getContextPath() + "/Login.jsp");
			}else if(request.getParameter("pageRedirect").compareTo("dashboard") == 0){
				//response.sendRedirect(request.getContextPath() + "/Dashboard.jsp");
				//request.setAttribute("page", request.getContextPath() + "/Dashboard.jsp");
				response.getWriter().write(request.getContextPath() + "/Dashboard.jsp");
			}else if(request.getParameter("pageRedirect").compareTo("fleet") == 0){
				//response.sendRedirect(request.getContextPath() + "/Fleet.jsp");
				response.getWriter().write(request.getContextPath() + "/Fleet.jsp");
			}else if(request.getParameter("pageRedirect").compareTo("reports") == 0){
				//response.sendRedirect(request.getContextPath() + "/Reports.jsp");
				response.getWriter().write(request.getContextPath() + "/Reports.jsp");
			}else if(request.getParameter("pageRedirect").compareTo("admin") == 0){
				//response.sendRedirect(request.getContextPath() + "/Admin.jsp");
				response.getWriter().write(request.getContextPath() + "/Admin.jsp");
			}
		}else if(request.getParameter("oper").compareTo("loadData") == 0){
			try{
				DatabaseController dbc = DatabaseController.getDBController();
				//System.out.println(dbc.getTableNames());
				//dbc.printResultSet(dbc.executeQuery("SELECT * FROM vehicle_emmission_2016"));
				ResultSet rs = dbc.executeQuery(request.getParameter("query"));
				ArrayList<ArrayList<String>> results = dbc.convertToArrayList(rs);
				//System.out.println(results);
				response.getWriter().write(results.toString());
			}catch(Exception e){
				
			}
		}else if(request.getParameter("oper").compareTo("login") == 0){
			//System.out.println("inside login");
			String username = request.getParameter("username");
	        String password = request.getParameter("password");
	        //System.out.println(username);
	        //System.out.println(password);
	        //System.out.println(request.getContextPath());
	        HttpSession session = request.getSession();
	        session.setAttribute("username",username);
	        session.setAttribute("password",password);
	        //response.sendRedirect("Dashboard.jsp");
	        response.getWriter().write(request.getContextPath() + "/Dashboard.jsp");
		}
	}

}
