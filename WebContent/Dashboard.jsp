<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<script src="js/common.js"></script>
<script src="js/dashboard.js"></script>
<script src="js/plugins/Chart/Chart.js"></script>
<script src="js/plugins/Chart/Chart.min.js"></script>
<script src ="js/addons/jquery-2.1.4.min.js"></script>
<script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<link rel="stylesheet" type="text/css" href="css/dashboard.css" />
<link rel="stylesheet" type="text/css" href="css/common.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" />

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Dashboard</title>
</head>
<%
String user = session.getAttribute("username").toString(); 
%>

<body>

	<div class ='Toast' style = 'display:none'>Toast Message Div</div>
	
	<div id="content">
		<div class="NavigationPanel">
			<div id="ButtonArea">
				<img id="dashboardButton" class="DashboardButton" src="images/dashboard2.png"
					title="Dashboard" onclick='dashboardButtons("dashboard")'>
				<img id="fleetButton" class="DashboardButton" src="images/fleet2.png"
					title="Fleet" onclick='dashboardButtons("fleet")'>
				<img id="reportsButton" class="DashboardButton" src="images/reports2.png"
					title="Reports" onclick='dashboardButtons("reports")'>
				<img id="adminButton" class="DashboardButton" src="images/admin2.png"
					title="Admin" onclick='dashboardButtons("admin")'>
			</div>
		</div>

		<div id ="MainDashboard">
			<div id ="DashboardAppHeader">
				<img id ="CharlotteLogo" src="images/Crown-Logo-Green.png"> <label 
					id ="AppTitle">Charlotte Fleet Analysis System</label> 
			</div>
			<div id ="DashboardHeader">
				<h3>
				<span style = "float:left; width:33%; text-align:left;">&nbsp;</span>
				<span style = "float:left; width:33%; text-align:center;">Dashboard - <% out.print(user); %></span>
				<span style = "float:left; width:33%; text-align:right;"><a href="Login.jsp"><button>Sign Out</button></a></span>
				</h3>	
			</div>
			<div id="dashboard_div">
				<div id="DashboardTop">
					<div id="Top1" class="DashboardTopPanels">
						<div id="top1-area">
							<div id="top1-chart-area" style="width: 100%; height: 100%;"></div>
						</div>
					</div>
					<div id="Top2" class="DashboardTopPanels">
						<div id="top2-area">
							<div style="width: 100%; height: 100%; text-align: center;">
									<h3>Top 5 Vehicles with Highest CO2 Emissions</h3>
									<h3>Year: </h3>
									<select id="yearSelection">
									</select>
								</div>
						</div>
					</div>
					<div id="Top3" class="DashboardTopPanels">
						<div id="top3-area">
							<div id="top3-chart-area" style="width: 100%; height: 100%;"></div>
						</div>
					</div>
				</div>
				<div id="DashboardBottom">
					<div id="DashboardBottomArea">
						<div id="DashboardFleetTable"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
<script>loadDashboard();</script>

</html>