<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<script src="js/common.js"></script>
<script src="js/dashboard.js"></script>
<script src = "js/fleet.js"></script>
<script src="js/plugins/Chart.js"></script>
<script src="js/plugins/Chart.min.js"></script>
<script src ="js/addons/jquery-2.1.4.min.js"></script>
<script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<link rel="stylesheet" type="text/css" href="css/fleet.css" />
<link rel="stylesheet" type="text/css" href="css/common.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" />

<div class ='Toast' style = 'display:none'>Toast Message Div</div>
	
	<div id="content">
		<div class="NavigationPanel">
			<div id="ButtonArea">
				<img id="dashboardButton" class="DashboardButton" src="images/dashboard2.png"
					title="Dashboard" onclick='dashboardButtons("dashboard")'>
				<img id="fleetButton" class="DashboardButton" src="images/fleet2.png"
					title="Fleet" onclick='dashboardButtons("fleet")'>
				<img id="reportsButton" class="DashboardButton" src="images/reports2.png"
					title="Reports" onclick='dashboardButtons("reports")'>
				<img id="adminButton" class="DashboardButton" src="images/admin2.png"
					title="Admin" onclick='dashboardButtons("admin")'>
			</div>
		</div>
		<div id="MainDashboard">
			<div id="DashboardAppHeader">
				<img id="CharlotteLogo" src="images/Crown-Logo-Green.png"> <label 
					id="AppTitle">Charlotte Fleet Analysis System</label> 
			</div>
			<div id="DashboardHeader">
				<h5>
					Fleet Information Table
				</h5> 
			</div>
		</div>
	<div id="FleetBottom">
		<div id="dashControl" style="float: left; width:100%">
			<div style="width:100%">
				<table style="float: left; text-align: right; width: 90%;">
					<tr>
						<td><div id="control1"></div></td>
						<td><div id="control2"></div></td>
						<td><div id="control3"></div></td>

					</tr>
					<tr>
						<td><div id="control4"></div></td>
						<td><div id="control5"></div></td>
						<td><div id="control6"></div></td>


					</tr>
					<tr>
						<td><div id="control7"></div></td>
						<td><div id="control8"></div></td>
						<td><div id="control9"></div></td>

					</tr>
					<tr>
						<td><div id="control10"></div></td>
						<td><div id="control11"></div></td>
						<td><div id="control12"></div></td>
					</tr>
					<tr>
						<td><div id="control13"></div></td>
					</tr>
				</table>
				<div>

					<div id="FleetBottomArea">
						<div id="FleetTableInformation"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
<script>loadFleetTable();</script>

</html>