<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Vehicle Fuel Economy Upload</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <form method="POST" action="uploadCsv" enctype="multipart/form-data">
            <p>Select a file: <input type="file" name="file" id="file" /></p>
            <input type="submit" value="Upload" name="upload" id="upload" />
        </form>
    </body>
</html>
