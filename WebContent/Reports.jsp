<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<script src="js/common.js"></script>
<script src="js/reports.js"></script>
<script src="js/Parse.js"></script>
<script src="js/plugins/Chart/Chart.js"></script>
<script src="js/plugins/Chart/Chart.min.js"></script>
<script src ="js/addons/jquery-2.1.4.min.js"></script>
<script src ="js/plugins/randomColor/randomColor.js"></script>
<script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<link rel="stylesheet" type="text/css" href="css/reports.css" />
<link rel="stylesheet" type="text/css" href="css/common.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" />

<title>Reports</title>
</head>
<body>

<div class ='Toast' style = 'display:none'>Toast Message Div</div>
	
	<div id="content">
		<div class="NavigationPanel">
			<div id="ButtonArea">
				<img id="dashboardButton" class="DashboardButton" src="images/dashboard2.png"
					title="Dashboard" onclick='dashboardButtons("dashboard")'>
				<img id="fleetButton" class="DashboardButton" src="images/fleet2.png"
					title="Fleet" onclick='dashboardButtons("fleet")'>
				<img id="reportsButton" class="DashboardButton" src="images/reports2.png"
					title="Reports" onclick='dashboardButtons("reports")'>
				<img id="adminButton" class="DashboardButton" src="images/admin2.png"
					title="Admin" onclick='dashboardButtons("admin")'>
			</div>
		</div>
		<div id="MainDashboard">
			<div id="DashboardAppHeader">
				<img id="CharlotteLogo" src="images/Crown-Logo-Green.png"> <label 
					id="AppTitle">Charlotte Fleet Analysis System</label> 
			</div>
			<div id="DashboardHeader">
				<h4>
					Reports
				</h4> 
			</div>
			<div id="ReportsTop">
				<div style="margin: 5px;">
					<label class="reportsLabels">Select Report: </label> <select
						id="reportsSelection">
						<option value="0">-Select-</option>
					</select>
				</div>
			</div>
			<div id="ReportsMain">
				
			</div>
		</div>
	</div>
</body>

<script>
Parse.initialize("RANidXGcAuzThiBR5UxZCoBbC9rdrl7cUrUC7gyX", "u53ugegUiYTrt6MAcPcJvtWyqsOvdT2Yyrhms0QT");
loadReportsPage();
</script>
</html>