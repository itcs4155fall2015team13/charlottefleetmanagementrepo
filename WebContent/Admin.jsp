<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<script src="js/common.js"></script>
<script src="js/admin.js"></script>
<script src="js/Parse.js"></script>
<script src="js/plugins/Chart.js"></script>
<script src="js/plugins/Chart.min.js"></script>
<script src ="js/addons/jquery-2.1.4.min.js"></script>
<script src="https://cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/admin.css" />
<link rel="stylesheet" type="text/css" href="css/common.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css" />
<title>Admin</title>
</head>
<body>

<div class ='Toast' style = 'display:none'>Toast Message Div</div>
	
	<div id="content">
		<div class="NavigationPanel">
			<div id="ButtonArea">
				<img id="dashboardButton" class="DashboardButton" src="images/dashboard2.png"
					title="Dashboard" onclick='dashboardButtons("dashboard")'>
				<img id="fleetButton" class="DashboardButton" src="images/fleet2.png"
					title="Fleet" onclick='dashboardButtons("fleet")'>
				<img id="reportsButton" class="DashboardButton" src="images/reports2.png"
					title="Reports" onclick='dashboardButtons("reports")'>
				<img id="adminButton" class="DashboardButton" src="images/admin2.png"
					title="Admin" onclick='dashboardButtons("admin")'>
			</div>
		</div>
		<div id="MainDashboard">
			<div id="DashboardAppHeader">
				<img id="CharlotteLogo" src="images/Crown-Logo-Green.png"> <label 
					id="AppTitle">Charlotte Fleet Analysis System</label> 
			</div>
			<div id="DashboardHeader">
				<h6>
					Administration
				</h6> 
			</div>
		</div>
			<div class = "buttonLocation">
				<input id = "Add User" type = "button" value = "Add User" class = "buttonLocation" onclick = "return addNewUser()">
				<input id = "Change Email" type = "button" value = "Change Email" class = "buttonLocation" onclick = " resetEmail()">
				<input id = "Change Password" type = "button" value = "Change Password" class = "buttonLocation" onclick = "return setPassword()">
				<input id = "Import Data" type = "button" value = "Import Data" class = "buttonLocation" onclick="location.href = 'FuelEconomyDataUploader.jsp';">
				<input id = "Export Data" type = "button" value = "Export Data" class = "buttonLocation" onclick="location.href = 'FuelEconomyDataExporter';">
			</div>
	</div>
</body>

<script>
Parse.initialize("RANidXGcAuzThiBR5UxZCoBbC9rdrl7cUrUC7gyX", "u53ugegUiYTrt6MAcPcJvtWyqsOvdT2Yyrhms0QT");
</script>
</html>