/**
 * 
 */

function loadDashboard(){	
	google.load("visualization", "1.1", {packages:["corechart", "bar", "table", "controls"]});
	google.setOnLoadCallback(function(){
		//populateTopGraphs();
		//drawTableSQL();
		getYearList();
	});
}

function populateTopGraphs() {
  var data = google.visualization.arrayToDataTable([
    ['Task', 'Hours per Day'],
    ['Work',     11],
    ['Eat',      2],
    ['Commute',  2],
    ['Watch TV', 2],
    ['Sleep',    7]
  ]);
  
  var data2 = google.visualization.arrayToDataTable([
                                                    ['Year', 'Sales', 'Expenses', 'Profit'],
                                                    ['2014', 1000, 400, 200],
                                                    ['2015', 1170, 460, 250],
                                                    ['2016', 660, 1120, 300],
                                                    ['2017', 1030, 540, 350]
                                                    ]);

  var options = {
    pieHole: 0.4,
    pieSliceText: 'label',
    legend: 'none',
    width: '100%',
    height: '90%',
    chartArea:{
        left: 10,
        top: 10,
        width: '100%',
        height: '90%',
    }
  };
  
  var options2 = {
		  chart: {
			  title: 'Company Performance',
			  subtitle: 'Sales, Expenses, and Profit: 2014-2017',
		  }
  };

  var chart = new google.visualization.PieChart(document.getElementById('top1-chart-area'));
  var chart2 = new google.charts.Bar(document.getElementById('top3-chart-area'));
  
  chart.draw(data, options);
  chart2.draw(data2, options2);
  
  $("#top1-chart-area").hide();
  $("#top3-chart-area").hide();
  $("#top1-chart-area").fadeIn(1000);
  $("#top3-chart-area").fadeIn(1000);
  
}

function drawDashboardTable() {
    var queryString = encodeURIComponent('SELECT *');

    var query = new google.visualization.Query(
    		'https://docs.google.com/spreadsheets/d/1OeplrrkFcqtfT-52uf0o5ujRcrg2EdJOvuJO7pP9JzQ/gviz/tq?sheet=Sheet1&headers=1&tq='
    		+ queryString);
    query.send(handleQueryResponseTable);
  }

function handleQueryResponseTable(response) {
    if (response.isError()) {
      alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
      return;
    }

    var data = response.getDataTable();
    var options = {
			 showRowNumber: true, 
			 width: '100%', 
			 height: '100%',
			 page: 'enable',
			 pageSize: 10
	 }
    
    var chart = new google.visualization.Table(document.getElementById('DashboardFleetTable'));
    chart.draw(data, options);
    
    $("#content").css({'height' : '100%'});
  }

function drawTableSQL() {
	
	var queryString1 = "select id as [Vehicle ID], year as [Year], make as [Make], model as [Model], " + 
	"trany as [Transmission], cylinders as [Engine Cylinders], displ as [Engine Displacement in Liters], " +
	"tCharger as [Turbocharged], sCharger as [Supercharged], city08 as [MPG City], highway08 as [MPG Highway], " +
	"comb08 as [MPG Combined], co2TailpipeGpm as [Tailpipe CO2 in GPM] " +
	"from vehicles order by year desc, make, model, trany, cylinders, displ, tCharger, sCharger, city08, highway08, " +
	"comb08, co2TailpipeGpm;";
	
	$.ajax({
		 type: "POST",    //define the type of ajax call (POST, GET, etc)
		 url: "FleetManagement",   //The name of the script you are calling
		 async: true,
		 data: {oper: "loadData", query: queryString1},    //Your data you are sending to the script
		 success: function(data){
			 //console.log(data);   //Your resulting action

			 var results = JSON.parse(data);
			 //console.log(results);

			 var data = new google.visualization.DataTable();
			 
			 for(var i = 0; i < results[0].length; i++){
				 data.addColumn('string', results[0][i]);
			 }
			 //Skip headers
			 for(var i = 1; i < results.length; i++){
				 var row = [];
				 
				 for(var j = 0; j < results[i].length; j++){
					 row.push(results[i][j]);
				 }				
				 data.addRow(row);
			 }

			 var options = {
					 showRowNumber: true, 
					 width: '100%', 
					 height: '100%',
					 page: 'enable',
					 pageSize: 10
			 }

			 var table = new google.visualization.Table(document.getElementById('DashboardFleetTable'));

			 table.draw(data, options);

		 },
		 error: function(xhr){
			 alert("An error occured: " + xhr.status + " " + xhr.statusText);
		 }
	 });
	
  }

function getYearList(){
	$("#yearSelection").empty();
	//$("#yearSelection").append('<option value="0">-Select-</option>');
	
	var queryString1 = "select year from vehicles group by year order by year desc;";

	$.ajax({
		type: "POST",    //define the type of ajax call (POST, GET, etc)
		url: "FleetManagement",   //The name of the script you are calling
		data: {oper: "loadData", query: queryString1},    //Your data you are sending to the script
		success: function(data){
			//console.log(data);   //Your resulting action

			var results = JSON.parse(data);

			//Skip Headers
			for(var i = 1; i < results.length; i++){
				$("#yearSelection").append("<option onclick=\"drawDashboardTableSQL('" + (results[i][0]|0) + "')\" value=\"" + (results[i][0]|0) + "\">" + (results[i][0]|0) + "</option>");

			}
			
			drawDashboardTableSQL((results[1][0]|0))
		},
		error: function(xhr){
			alert("An error occured: " + xhr.status + " " + xhr.statusText);
		}
	});
}

function drawDashboardTableSQL(year) {
	
	var queryString1 = "select top 5 id as [Vehicle ID], year as [Year], make as [Make], model as [Model], " + 
	"trany as [Transmission], cylinders as [Engine Cylinders], displ as [Engine Displacement in Liters], " +
	"tCharger as [Turbocharged], sCharger as [Supercharged], city08 as [MPG City], highway08 as [MPG Highway], " +
	"comb08 as [MPG Combined], co2TailpipeGpm as [Tailpipe CO2 in GPM] " +
	"from vehicles where year = " + year + " " +
	"order by co2TailpipeGpm desc;";
	
	$.ajax({
		 type: "POST",    //define the type of ajax call (POST, GET, etc)
		 url: "FleetManagement",   //The name of the script you are calling
		 async: true,
		 data: {oper: "loadData", query: queryString1},    //Your data you are sending to the script
		 success: function(data1){
			 //console.log(data);   //Your resulting action

			 var results = JSON.parse(data1);
			 //console.log(results);

			 var data = new google.visualization.DataTable();
			 
			 data.addColumn('number', results[0][0]);
			 data.addColumn('number', results[0][1]);
			 
			 for(var i = 2; i < results[0].length; i++){
				 data.addColumn('string', results[0][i]);
			 }
			 //Skip headers
			 for(var i = 1; i < results.length; i++){
				 var row = [];
				 
				 row.push(parseInt(results[i][0]));
				 row.push(parseInt(results[i][1]));
				 
				 for(var j = 2; j < results[i].length; j++){
					 row.push(results[i][j]);
				 }				
				 data.addRow(row);
			 }

			 var options = {
					 showRowNumber: true, 
					 width: '100%', 
					 height: '100%',
					 page: 'enable',
					 pageSize: 10
			 }

			 var table = new google.visualization.Table(document.getElementById('DashboardFleetTable'));

			 table.draw(data, options);

			 drawDashboardPieChartSQL(year);
			 drawDashboardBarChartSQL(year);
		 },
		 error: function(xhr){
			 alert("An error occured: " + xhr.status + " " + xhr.statusText);
		 }
	 });
	
  }

function drawDashboardPieChartSQL(year) {
	
	var queryString1 = "select top 5 year as [Year], make as [Make], model as [Model], " +
	"trany as [Transmission], cylinders as [Engine Cylinders], displ as [Engine Displacement in Liters], " +
	"tCharger as [Turbocharged], sCharger as [Supercharged], co2TailpipeGpm as [Tailpipe CO2 in GPM] " +
	"from vehicles where year = " + year + " " +
	"order by co2TailpipeGpm desc;";
	
	$.ajax({
		 type: "POST",    //define the type of ajax call (POST, GET, etc)
		 url: "FleetManagement",   //The name of the script you are calling
		 async: true,
		 data: {oper: "loadData", query: queryString1},    //Your data you are sending to the script
		 success: function(data1){
			 //console.log(data);   //Your resulting action

			 var results = JSON.parse(data1);
			 //console.log(results);

			 var pieData1 = [];
			 
			 //Add headers
			 pieData1.push(['Vehicle', 'Tailpipe CO2']);
			 
			 //Skip Headers
			 for(var i = 1; i < results.length; i++){
				 var stringOptions = "" + results[i][0] + " " + results[i][1] + " " + results[i][2] + " - ";
				 if(results[i][3] != "null"){
					 stringOptions = stringOptions + results[i][3];
				 }
				 if(results[i][4] != "null"){
					 stringOptions = stringOptions + ", " + results[i][4] + "cyl";
				 }
				 if(results[i][5] != "null"){
					 stringOptions = stringOptions + ", " + results[i][5] + "L";
				 }
				 if(results[i][6] != "null"){
					 stringOptions = stringOptions + ", Turbo";
				 }
				 if(results[i][7] != "null"){
					 stringOptions = stringOptions + ", Supercharge";
				 }
				 pieData1.push([stringOptions, parseInt(results[i][8])]);

			 }
			 
			 //console.log(pieData1);
			 
			 var data = google.visualization.arrayToDataTable(pieData1);
			 
			 var options = {
					 pieHole: 0.4,
					 pieSliceText: 'value',
					 legend: 'none',
					 title: 'Tailpipe CO2',
					 width: '100%',
					 height: '80%',
					 chartArea:{
						 left: 10,
						 top: 20,
						 width: '100%',
						 height: '80%',
					 }
			 };

			 var chart = new google.visualization.PieChart(document.getElementById('top1-chart-area'));

			 chart.draw(data, options);

			 $("#top1-chart-area").hide();
			 $("#top1-chart-area").fadeIn(1000);
		 },
		 error: function(xhr){
			 alert("An error occured: " + xhr.status + " " + xhr.statusText);
		 }
	 });
	
  }

function drawDashboardBarChartSQL(year) {
	
	var queryString1 = "select top 5 year as [Year], make as [Make], model as [Model], " +
	"trany as [Transmission], cylinders as [Engine Cylinders], displ as [Engine Displacement in Liters], " +
	"tCharger as [Turbocharged], sCharger as [Supercharged], city08 as [MPG City], highway08 as [MPG Highway], " +
    "comb08 as [MPG Combined] " +
	"from vehicles where year = " + year + " " +
	"order by co2TailpipeGpm desc;";
	
	$.ajax({
		 type: "POST",    //define the type of ajax call (POST, GET, etc)
		 url: "FleetManagement",   //The name of the script you are calling
		 async: true,
		 data: {oper: "loadData", query: queryString1},    //Your data you are sending to the script
		 success: function(data1){
			 //console.log(data);   //Your resulting action

			 var results = JSON.parse(data1);
			 //console.log(results);

			 var pieData1 = [];
			 var header = ['MPG'];
			 var row1 = ['City'];
			 var row2 = ['Highway'];
			 var row3 = ['Combined'];
			 
			 //Skip Headers
			 for(var i = 1; i < results.length; i++){
				 var stringOptions = "" + results[i][0] + " " + results[i][1] + " " + results[i][2] + " - \n";
				 if(results[i][3] != "null"){
					 stringOptions = stringOptions + results[i][3];
				 }
				 if(results[i][4] != "null"){
					 stringOptions = stringOptions + ", " + results[i][4] + "cyl";
				 }
				 if(results[i][5] != "null"){
					 stringOptions = stringOptions + ", " + results[i][5] + "L";
				 }
				 if(results[i][6] != "null"){
					 stringOptions = stringOptions + ", Turbo";
				 }
				 if(results[i][7] != "null"){
					 stringOptions = stringOptions + ", Supercharge";
				 }
				 header.push(stringOptions);
				 row1.push(parseInt(results[i][8]));
				 row2.push(parseInt(results[i][9]));
				 row3.push(parseInt(results[i][10]));

			 }
			 
			 pieData1.push(header);
			 pieData1.push(row1);
			 pieData1.push(row2);
			 pieData1.push(row3);
			 
			 //console.log(pieData1);
			 
			 var data2 = google.visualization.arrayToDataTable(pieData1);



			 var options2 = {
					 legend: { position: "none" },
					 chart: {
						 title: 'MPG'
					 }
			 };

			 var chart2 = new google.charts.Bar(document.getElementById('top3-chart-area'));

			 chart2.draw(data2, options2);

			 $("#top3-chart-area").hide();
			 $("#top3-chart-area").fadeIn(1000);
			 
			                                                 
			 
		 },
		 error: function(xhr){
			 alert("An error occured: " + xhr.status + " " + xhr.statusText);
		 }
	 });
	
  }