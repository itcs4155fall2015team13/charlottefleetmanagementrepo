/**
 * 
 */
 
 function dashboardButtons(buttonName){
	 console.log(buttonName);
	 	 
	 //$.post("FleetManagement", {pageRedirect: buttonName});
	 $.ajax({
		 type: "POST",    //define the type of ajax call (POST, GET, etc)
		 url: "FleetManagement",   //The name of the script you are calling
		 data: {oper: "pageRedirect", pageRedirect: buttonName},    //Your data you are sending to the script
		 success: function(data){
			 console.log(data);   //Your resulting action
			 window.location.href = window.location.origin + data;
		 },
		 error: function(xhr){
			 alert("An error occured: " + xhr.status + " " + xhr.statusText);
		 }
	 });
 }
 
 function logout(){
	 Parse.initialize("mazjpfYKdEc7n7hOYffvCHJxO85vD47A6A133ZPy", "pxdM7U4u6kVauGpS74Qm03RcBNWDQdGJdtzlupPi");
	 Parse.User.logOut();
 }
 
 function ShowToast(message, length, isError){
     //console.log("isError: " + isError);
	 $('.Toast').stop();
	 
     if(isError == true){
            $('.Toast').css("background-color", "rgba(195, 4, 4, 0.9)");
     }else{
    	 $('.Toast').css("background-color", "rgba(8, 195, 4, 0.93)");
     }
     if(typeof length == 'undefined')
            length = 5;
    
     $('.Toast').text(message).fadeIn(400).delay(length * 1000).fadeOut(400);
}