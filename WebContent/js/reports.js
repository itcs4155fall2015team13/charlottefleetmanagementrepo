/**
 * 
 */
var chart;
var chartSecondary;

function loadReportsPage(){
	google.load("visualization", "1.1", {packages:["corechart", "bar", "table", "controls"]});
	google.setOnLoadCallback(loadReportsList);
	
}

function loadReportsList(){
//var reportList = [["Emmissions", "loadReport1()"], ["Vehicle Type", "loadReport2()"], ["Vehicle Comparison", "loadReport3()"]];
var reportList = [["Vehicle Comparison", "loadReport3()"]];
	
	for(var i = 0; i < reportList.length; i++){
		$("#reportsSelection").append("<option onclick=\"" + reportList[i][1] + "\">" + reportList[i][0] + "</option>");
	}
}

function loadReport1(){
	$("#ReportsMain").empty();
	
	$("#ReportsMain").append(
	"<div id=\"MainLeftSide\"> " +
	"<div id=\"MainLeftSideArea\">" + 
	"<form style=\"text-align:left\">" +
	"<input type=\"radio\" name=\"reportOptions\" value=\"count\" checked=\"checked\">Count</input>" +
	"<br>" +
	"<input type=\"radio\" name=\"reportOptions\" value=\"count\">Mileage</input>" +
	"<br>" +
	"<input type=\"radio\" name=\"reportOptions\" value=\"count\">CO2</input>" +
	"<br>" +
	"<input type=\"radio\" name=\"reportOptions\" value=\"count\">NOx</input>" +
	"</form>" +
	"</div>" +
	"</div>" +
	"<div id=\"MainCenterSide\"> " +
	"<div id=\"MainCenterSideArea\">" + 
	"<div id=\"chart-area1\" > </div>" +
	"</div>" +
	"</div>" +
	"<div id=\"MainRightSide\">" +
	"<div id=\"MainRightSideArea\">" +
	"<div id=\"chart-area2\" > </div>" +
	"</div>" +
	"</div>"
	);
	
	var queryString1 = "Select a.Vehicle_Type, COUNT(*) as NumOfVehicles from( " +
			"select Test_Vehicle_ID, Vehicle_Type from vehicle_emmission_2016 " +
			"GROUP BY Test_Vehicle_ID, Vehicle_Type) a " +
			"GROUP By a.Vehicle_TYPE";
	
	var queryString2 = "Select a.Represented_Test_Veh_Make, COUNT(*) as NumOfVehicles from( " +
			"select Test_Vehicle_ID, Represented_Test_Veh_Make from vehicle_emmission_2016 " +
			"GROUP BY Test_Vehicle_ID, Represented_Test_Veh_Make) a " +
			"GROUP By a.Represented_Test_Veh_Make";
	
	$.ajax({
		 type: "POST",    //define the type of ajax call (POST, GET, etc)
		 url: "FleetManagement",   //The name of the script you are calling
		 data: {oper: "loadData", query: queryString1},    //Your data you are sending to the script
		 success: function(data){
			 //console.log(data);   //Your resulting action
			 
			 var results = JSON.parse(data);
			 //console.log(results);
			 
			 var pieData1 = [];
			 
			 //Add headers
			 pieData1.push([results[0][0], results[0][1]]);
			 
			 //Skip Headers
			 for(var i = 1; i < results.length; i++){
				 pieData1.push([results[i][0], parseInt(results[i][1])]);
				 
			 }
			 
			 //console.log(pieData1);
			 
			 var data1 = google.visualization.arrayToDataTable(pieData1);
			
			 var options1 = {
					    pieHole: 0,
					    pieSliceText: 'label',
					    title: "Count by Type",
					    width: '100%',
					    height: '100%',
					    chartArea:{
					        left: 5,
					        top: 20,
					        width: '100%',
					        height: '100%',
					    }
					    //legend: 'none'
					  };
			 
			 var chart1 = new google.visualization.PieChart(document.getElementById('chart-area1'));
			 chart1.draw(data1, options1);
			 $("#chart-area1").hide();
			 $("#chart-area1").fadeIn(1000);
				
		 },
		 error: function(xhr){
			 alert("An error occured: " + xhr.status + " " + xhr.statusText);
		 }
	 });
	
	$.ajax({
		 type: "POST",    //define the type of ajax call (POST, GET, etc)
		 url: "FleetManagement",   //The name of the script you are calling
		 data: {oper: "loadData", query: queryString2},    //Your data you are sending to the script
		 success: function(data){
			 //console.log(data);   //Your resulting action
			 
			 var results = JSON.parse(data);
			 //console.log(results);
			 
			 var doughnutData2 = [];
			 
			//Add headers
			 doughnutData2.push([results[0][0], results[0][1]]);
			 
			 //Skip Headers
			 for(var i = 1; i < results.length; i++){
				 doughnutData2.push([results[i][0], parseInt(results[i][1])]);
				 
			 }
			 
			 //console.log(doughnutData2);
			 
			 var data2 = google.visualization.arrayToDataTable(doughnutData2);
			
			 var options2 = {
					    pieHole: 0.4,
					    pieSliceText: 'label',
					    title: "Count by Manufacturer",
					    //legend: 'none',
					    width: '100%',
					    height: '100%',
					    chartArea:{
					        left: 5,
					        top: 20,
					        width: '100%',
					        height: '100%',
					    }
					  };
			 
			 var chart2 = new google.visualization.PieChart(document.getElementById('chart-area2'));
			 chart2.draw(data2, options2);
			 $("#chart-area2").hide();
			 $("#chart-area2").fadeIn(1000);
				
		 },
		 error: function(xhr){
			 alert("An error occured: " + xhr.status + " " + xhr.statusText);
		 }
	 });
}

function loadReport1_1(){
//	$("#ReportsMain").empty();
//	
//	$("#ReportsMain").append(
//	"<div id=\"MainLeftSide\"> " +
//	"<div id=\"MainLeftSideArea\">" + 
//	"<form style=\"text-align:left\">" +
//	"<input type=\"radio\" name=\"reportOptions\" value=\"count\" checked=\"checked\">Count</input>" +
//	"<br>" +
//	"<input type=\"radio\" name=\"reportOptions\" value=\"count\">Mileage</input>" +
//	"<br>" +
//	"<input type=\"radio\" name=\"reportOptions\" value=\"count\">CO2</input>" +
//	"<br>" +
//	"<input type=\"radio\" name=\"reportOptions\" value=\"count\">NOx</input>" +
//	"</form>" +
//	"</div>" +
//	"</div>" +
//	"<div id=\"MainCenterSide\"> " +
//	"<div id=\"MainCenterSideArea\">" + 
//	"<div id=\"chart-area1\" > </div>" +
//	"</div>" +
//	"</div>" +
//	"<div id=\"MainRightSide\">" +
//	"<div id=\"MainRightSideArea\">" +
//	"<div id=\"chart-area2\" > </div>" +
//	"</div>" +
//	"</div>"
//	);
	
	var queryString1 = encodeURIComponent("Select a.Vehicle_Type, COUNT(*) as NumOfVehicles from( " +
			"select Test_Vehicle_ID, Vehicle_Type from vehicle_emmission_2016 " +
			"GROUP BY Test_Vehicle_ID, Vehicle_Type) a " +
			"GROUP By a.Vehicle_TYPE");
	var queryString2 = encodeURIComponent('SELECT *');

    var query1 = new google.visualization.Query(
    		'https://docs.google.com/spreadsheets/d/1OeplrrkFcqtfT-52uf0o5ujRcrg2EdJOvuJO7pP9JzQ/gviz/tq?sheet=Sheet1&headers=1&tq='
    		+ queryString);
    var query2 = new google.visualization.Query(
    		'https://docs.google.com/spreadsheets/d/1OeplrrkFcqtfT-52uf0o5ujRcrg2EdJOvuJO7pP9JzQ/gviz/tq?sheet=Sheet1&headers=1&tq='
    		+ queryString);
    query1.send(loadReport1_grid1);
    query2.send(loadReport1_grid2);
    
}

function loadReport1_grid1(){
	
}

function loadReport1_grid2(){
	
}

function loadReport2(){
	$("#ReportsMain").empty();
	
	if(chart != null){
		//console.log("got here");
		chart.clear();
		chart.destroy();
	}
	
	if(chartSecondary != null){
		//console.log("got here");
		chartSecondary.clear();
		chartSecondary.destroy();
	}
	
	$("#ReportsMain").append(
			"<div id=\"MainSide\">" +
			"<div id=\"MainSideArea\">" +
			"<canvas id=\"chart-area\" width=\"500\" height=\"500\"> </canvas>" +
			"</div>" +
			"</div>"
			);;
	
	var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

	var barChartData = {
		labels : ["TAHOE","ASTRO","BLAZER","COLORADO","CAVALIER","IMPALA","SILVERADO"],
		datasets : [
			{
				fillColor : "rgba(220,220,220,0.5)",
				strokeColor : "rgba(220,220,220,0.8)",
				highlightFill: "rgba(220,220,220,0.75)",
				highlightStroke: "rgba(220,220,220,1)",
				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
			},
			{
				fillColor : "rgba(151,187,205,0.5)",
				strokeColor : "rgba(151,187,205,0.8)",
				highlightFill : "rgba(151,187,205,0.75)",
				highlightStroke : "rgba(151,187,205,1)",
				data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
			}
		]

	}
	
	var ctx = document.getElementById("chart-area").getContext("2d");
	chart = new Chart(ctx).Bar(barChartData, {
		responsive : false
	});
	chart.update();
}

function loadReport3(){
	$("#ReportsMain").empty();
	
	$("#ReportsMain").load("reports/VehicleComparison.html");

	getYearList('1');
	getYearList('2');
	
//	var queryString1 = "SELECT Test_Vehicle_ID FROM vehicle_emmission_2016 GROUP BY Test_Vehicle_ID ORDER BY Test_Vehicle_ID;";
//
//	$.ajax({
//		type: "POST",    //define the type of ajax call (POST, GET, etc)
//		url: "FleetManagement",   //The name of the script you are calling
//		data: {oper: "loadData", query: queryString1},    //Your data you are sending to the script
//		success: function(data){
//			//console.log(data);   //Your resulting action
//
//			var results = JSON.parse(data);
//
//			//Skip Headers
//			for(var i = 1; i < results.length; i++){
//				$("#Report3VehicleList1").append("<option onclick=\"vehicleComparisonDataPull('" + results[i][0] + "', 'Report3Table1')\" value=\"" + results[i][0] + "\">" + results[i][0] + "</option>");
//				$("#Report3VehicleList2").append("<option onclick=\"vehicleComparisonDataPull('" + results[i][0] + "', 'Report3Table2')\" value=\"" + results[i][0] + "\">" + results[i][0] + "</option>");
//
//			}
//		},
//		error: function(xhr){
//			alert("An error occured: " + xhr.status + " " + xhr.statusText);
//		}
//	});
}

function vehicleComparisonDataPull(vehicleID, tableDiv){
	
	var queryString1 = "SELECT * FROM vehicle_emmission_2016 WHERE Test_Vehicle_ID = '" + vehicleID + "';";

	$.ajax({
		type: "POST",    //define the type of ajax call (POST, GET, etc)
		url: "FleetManagement",   //The name of the script you are calling
		data: {oper: "loadData", query: queryString1},    //Your data you are sending to the script
		success: function(data){
			//console.log(data);   //Your resulting action

			var results = JSON.parse(data);

			var data = new google.visualization.DataTable();
			 
			 for(var i = 0; i < results[0].length; i++){
				 data.addColumn('string', results[0][i]);
			 }
			 //Skip headers
			 for(var i = 1; i < results.length; i++){
				 var row = [];
				 
				 for(var j = 0; j < results[i].length; j++){
					 row.push(results[i][j]);
				 }				
				 data.addRow(row);
			 }
			 
			 //var cssClasses = {headerRow: 'tblHeaderClass',tableRow: 'tblRowClass'};

			 var options = {
					 showRowNumber: true, 
					 width: '100%', 
					 height: '300px',
					 page: 'enable',
					 pageSize: 5//,
//					 allowHTML: true, 
//					 'cssClassNames':cssClasses
			 }
			 
			 console.log(tableDiv);

			 var table = new google.visualization.Table(document.getElementById(tableDiv));

			 table.draw(data, options);
		},
		error: function(xhr){
			alert("An error occured: " + xhr.status + " " + xhr.statusText);
		}
	});
}

function getYearList(selectList){
	$("#Report3MakeList" + selectList).empty();
	$("#Report3ModelList" + selectList).empty();
	$("#Report3OptionsList" + selectList).empty();
	$("#Report3MakeList" + selectList).append('<option value="-Select-">-Select-</option>');
	$("#Report3ModelList" + selectList).append('<option value="-Select-">-Select-</option>');
	$("#Report3OptionsList" + selectList).append('<option value="-Select-">-Select-</option>');
	$("#Report3MakeList" + selectList).val("-Select-");
	$("#Report3ModelList" + selectList).val("-Select-");
	$("#Report3OptionsList" + selectList).val("-Select-");
	$("#Report3MakeList" + selectList).prop("disabled", true);
	$("#Report3ModelList" + selectList).prop("disabled", true);
	$("#Report3OptionsList" + selectList).prop("disabled", true);
	hideVehicleComparisonInfo(selectList);
	
	var queryString1 = "select year from vehicles group by year order by year desc;";

	$.ajax({
		type: "POST",    //define the type of ajax call (POST, GET, etc)
		url: "FleetManagement",   //The name of the script you are calling
		data: {oper: "loadData", query: queryString1},    //Your data you are sending to the script
		success: function(data){
			//console.log(data);   //Your resulting action

			var results = JSON.parse(data);

			//Skip Headers
			for(var i = 1; i < results.length; i++){
				$("#Report3YearList" + selectList).append("<option onclick=\"getMakeList('" + (results[i][0]|0) + "', '" + selectList + "')\" value=\"" + (results[i][0]|0) + "\">" + (results[i][0]|0) + "</option>");

			}
		},
		error: function(xhr){
			alert("An error occured: " + xhr.status + " " + xhr.statusText);
		}
	});
}

function getMakeList(year, selectList){
	$("#Report3MakeList" + selectList).empty();
	$("#Report3ModelList" + selectList).empty();
	$("#Report3OptionsList" + selectList).empty();
	$("#Report3ModelList" + selectList).append('<option value="-Select-">-Select-</option>');
	$("#Report3MakeList" + selectList).append('<option value="-Select-">-Select-</option>');
	$("#Report3OptionsList" + selectList).append('<option value="-Select-">-Select-</option>');
	$("#Report3ModelList" + selectList).val("-Select-");
	$("#Report3OptionsList" + selectList).val("-Select-");
	$("#Report3ModelList" + selectList).prop("disabled", true);
	$("#Report3OptionsList" + selectList).prop("disabled", true);
	hideVehicleComparisonInfo(selectList);
	
	var queryString1 = "select make from vehicles where year = " + year + " group by make order by make;";

	$.ajax({
		type: "POST",    //define the type of ajax call (POST, GET, etc)
		url: "FleetManagement",   //The name of the script you are calling
		data: {oper: "loadData", query: queryString1},    //Your data you are sending to the script
		success: function(data){
			//console.log(data);   //Your resulting action

			var results = JSON.parse(data);

			//Skip Headers
			for(var i = 1; i < results.length; i++){
				$("#Report3MakeList" + selectList).append("<option onclick=\"getModelList('" + results[i][0] + "','" + year + "' , '" + selectList + "')\" value=\"" + results[i][0] + "\">" + results[i][0] + "</option>");

				$("#Report3MakeList" + selectList).prop("disabled", false);
			}
		},
		error: function(xhr){
			alert("An error occured: " + xhr.status + " " + xhr.statusText);
		}
	});
}

function getModelList(make, year, selectList){
	$("#Report3ModelList" + selectList).empty();
	$("#Report3OptionsList" + selectList).empty();
	$("#Report3ModelList" + selectList).append('<option value="-Select-">-Select-</option>');
	$("#Report3OptionsList" + selectList).append('<option value="-Select-">-Select-</option>');
	$("#Report3OptionsList" + selectList).val("-Select-");
	$("#Report3OptionsList" + selectList).prop("disabled", true);
	hideVehicleComparisonInfo(selectList);
	
	var queryString1 = "select model from vehicles where year = " + year + " and lower(make) like '" + make.toLowerCase() + "' group by model;";

	$.ajax({
		type: "POST",    //define the type of ajax call (POST, GET, etc)
		url: "FleetManagement",   //The name of the script you are calling
		data: {oper: "loadData", query: queryString1},    //Your data you are sending to the script
		success: function(data){
			//console.log(data);   //Your resulting action

			var results = JSON.parse(data);

			//Skip Headers
			for(var i = 1; i < results.length; i++){
				$("#Report3ModelList" + selectList).append("<option onclick=\"getOptionsList('" + results[i][0] + "','" + make + "' ,'" + year + "' , '" + selectList + "')\" value=\"" + results[i][0] + "\">" + results[i][0] + "</option>");

				$("#Report3ModelList" + selectList).prop("disabled", false);
			}
		},
		error: function(xhr){
			alert("An error occured: " + xhr.status + " " + xhr.statusText);
		}
	});
}

function getOptionsList(model, make, year, selectList){
	$("#Report3OptionsList" + selectList).empty();
	$("#Report3OptionsList" + selectList).append('<option value="-Select-">-Select-</option>');
	hideVehicleComparisonInfo(selectList);
	
	var queryString1 = "select id, trany, cylinders, displ, tCharger, sCharger " +
				"from vehicles where year = " + year + " and lower(make) like '" + make.toLowerCase() + "' and lower(model) like '" + model.toLowerCase() + "';";

	$.ajax({
		type: "POST",    //define the type of ajax call (POST, GET, etc)
		url: "FleetManagement",   //The name of the script you are calling
		data: {oper: "loadData", query: queryString1},    //Your data you are sending to the script
		success: function(data){
			//console.log(data);   //Your resulting action

			var results = JSON.parse(data);

			//Skip Headers
			for(var i = 1; i < results.length; i++){
				var stringOptions = "";
				if(results[i][1] != "null"){
					stringOptions = stringOptions + results[i][1];
				}
				if(results[i][2] != "null"){
					stringOptions = stringOptions + ", " + results[i][2] + "cyl";
				}
				if(results[i][3] != "null"){
					stringOptions = stringOptions + ", " + results[i][3] + "L";
				}
				if(results[i][4] != "null"){
					stringOptions = stringOptions + ", Turbo";
				}
				if(results[i][5] != "null"){
					stringOptions = stringOptions + ", Supercharge";
				}
				
				$("#Report3OptionsList" + selectList).append("<option onclick=\"getVehicleData('" + results[i][0] + "','" + selectList + "')\" value=\"" + results[i][0] + "\">" + stringOptions + "</option>");

				$("#Report3OptionsList" + selectList).prop("disabled", false);
			}
		},
		error: function(xhr){
			alert("An error occured: " + xhr.status + " " + xhr.statusText);
		}
	});
}

function getVehicleData(id, selectList){
	hideVehicleComparisonInfo(selectList);
	
	var queryString1 = "select city08, highway08, comb08, co2TailpipeGpm from vehicles where id = '" + id + "';";

	$.ajax({
		type: "POST",    //define the type of ajax call (POST, GET, etc)
		url: "FleetManagement",   //The name of the script you are calling
		data: {oper: "loadData", query: queryString1},    //Your data you are sending to the script
		success: function(data){
			console.log(data);

			var results = JSON.parse(data);

//			Skip Headers
//			for(var i = 1; i < results.length; i++){
//				
//			}
			
			console.log(results[1]);
			$("#mpgCity" + selectList).text(results[1][0]);
			$("#mpgHighway" + selectList).text(results[1][1]);
			$("#mpgCombined" + selectList).text(results[1][2]);
			$("#tailpipeCO2" + selectList).text(results[1][3]);
			
			$("#Report3Info" + selectList).show();
			
			if($("#Report3Info1").css('display') == 'block' && $("#Report3Info2").css('display') == 'block'){
				displayVehicleComparsionGraph();
			}
		},
		error: function(xhr){
			alert("An error occured: " + xhr.status + " " + xhr.statusText);
		}
	});
}

function hideVehicleComparisonInfo(selectList){
	$("#Report3Info" + selectList).hide();
	$("#mpgCity" + selectList).val('');
	$("#mpgHighway" + selectList).val('');
	$("#mpgCombined" + selectList).val('');
	$("#tailpipeCO2" + selectList).val('');
	$("#Report3BarChart").hide();
	$("#Report3BarChart2").hide();
}

function displayVehicleComparsionGraph(){
	var data1 = google.visualization.arrayToDataTable([
	                                                   ['MPG', 'Vehicle 1', 'Vehicle 2'],
	                                                   ['City', parseInt($("#mpgCity1").text()), parseInt($("#mpgCity2").text())],
	                                                   ['Highway', parseInt($("#mpgHighway1").text()), parseInt($("#mpgHighway2").text())],
	                                                   ['Combined', parseInt($("#mpgCombined1").text()), parseInt($("#mpgCombined2").text())]
	                                                   ]);
	
	var data2 = google.visualization.arrayToDataTable([
	                                                   ['Vehicle', 'Tailpipe CO2'],
	                                                   //['Tailpipe CO2', parseInt($("#tailpipeCO21").text()), parseInt($("#tailpipeCO22").text())]
	                                                   ['Vehicle 1', parseInt($("#tailpipeCO21").text())],
	                                                   ['Vehicle 2', parseInt($("#tailpipeCO22").text())]
	                                                   ]);
	var options2 = {
		    pieHole: 0,
		    pieSliceText: 'label',
		    pieSliceTextStyle: {
		    	color: 'black'
		    },
		    title: "Tailpipe CO2",
		    legend: 'none',
		    width: '100%',
		    height: '80%',
		    chartArea:{
		        left: 10,
		        top: 20,
		        width: '100%',
		        height: '80%',
		    }
		  };

	var chart1 = new google.charts.Bar(document.getElementById('Report3BarChart'));
	var chart2 = new google.visualization.PieChart(document.getElementById('Report3BarChart2'));
	  
	chart1.draw(data1);
	chart2.draw(data2, options2);

	$("#Report3BarChart").hide();	
	$("#Report3BarChart2").hide();
	$("#Report3BarChart").fadeIn(1000);
	$("#Report3BarChart2").fadeIn(1000);
	
	$("#content").css({'height' : '100%'});
}