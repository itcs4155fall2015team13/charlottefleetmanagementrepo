var googleSpreadsheetURL = 'https://docs.google.com/spreadsheets/d/1OeplrrkFcqtfT-52uf0o5ujRcrg2EdJOvuJO7pP9JzQ/gviz/tq?gid=0&headers=1&tq=';

/**
 * 
 */
function loadFleetTable(){
	google.load("visualization", "1.1", {packages:["table", "controls"]});
    google.setOnLoadCallback(drawTableSQL);
}

function drawTable() {
    var queryString = encodeURIComponent('SELECT *');

    var query = new google.visualization.Query(
    		'https://docs.google.com/spreadsheets/d/1OeplrrkFcqtfT-52uf0o5ujRcrg2EdJOvuJO7pP9JzQ/gviz/tq?sheet=Sheet1&headers=1&tq='
    		+ queryString);
    query.send(handleQueryResponse);
  }

function drawTableSQL() {
	
	var queryString1 = "select id as [Vehicle ID], year as [Year], make as [Make], model as [Model], " + 
		"trany as [Transmission], cylinders as [Engine Cylinders], displ as [Engine Displacement in Liters], " +
		"tCharger as [Turbocharged], sCharger as [Supercharged], city08 as [MPG City], highway08 as [MPG Highway], " +
		"comb08 as [MPG Combined], co2TailpipeGpm as [Tailpipe CO2 in GPM] " +
		"from vehicles order by year desc, make, model, trany, cylinders, displ, tCharger, sCharger, city08, highway08, " +
		"comb08, co2TailpipeGpm;";
	
	$.ajax({
		 type: "POST",    //define the type of ajax call (POST, GET, etc)
		 url: "FleetManagement",   //The name of the script you are calling
		 async: true,
		 data: {oper: "loadData", query: queryString1},    //Your data you are sending to the script
		 success: function(data){
			 //console.log(data);   //Your resulting action

			 var results = JSON.parse(data);
			 //console.log(results);

			 var data = new google.visualization.DataTable();
			 
			 for(var i = 0; i < results[0].length; i++){
				 data.addColumn('string', results[0][i]);
			 }
			 //Skip headers
			 for(var i = 1; i < results.length; i++){
				 var row = [];
				 
				 for(var j = 0; j < results[i].length; j++){
					 row.push(results[i][j]);
				 }				
				 data.addRow(row);
			 }

			 var options = {
					 showRowNumber: true, 
					 width: '100%', 
					 height: '100%',
					 page: 'enable',
					 pageSize: 15
			 }
			 

			 //var table = new google.visualization.Table(document.getElementById('FleetTableInformation'));

			 //table.draw(data, options);
			 
			 var dashboard = new google.visualization.Dashboard(document.getElementById('dashControl'));

			    var msgTable = new google.visualization.ChartWrapper({
			        chartType: 'Table',
			        containerId: 'FleetTableInformation',
			        options: options
			    });

			    var control1 = new google.visualization.ControlWrapper({
			        controlType: 'StringFilter',
			        containerId: 'control1',
			        options: {
			             filterColumnIndex: 0
			        }
			    });
			    
			    var control2 = new google.visualization.ControlWrapper({
			        controlType: 'StringFilter',
			        containerId: 'control2',
			        options: {
			             filterColumnIndex: 1
			        }
			    });
			    
			    var control3 = new google.visualization.ControlWrapper({
			        controlType: 'StringFilter',
			        containerId: 'control3',
			        options: {
			             filterColumnIndex: 2
			        }
			    });
			    
			    var control4 = new google.visualization.ControlWrapper({
			        controlType: 'StringFilter',
			        containerId: 'control4',
			        options: {
			             filterColumnIndex: 3
			        }
			    });
			    
			    var control5 = new google.visualization.ControlWrapper({
			        controlType: 'StringFilter',
			        containerId: 'control5',
			        options: {
			             filterColumnIndex: 4
			        }
			    });
			    
			    var control6 = new google.visualization.ControlWrapper({
			        controlType: 'StringFilter',
			        containerId: 'control6',
			        options: {
			             filterColumnIndex: 5
			        }
			    });
			    
			    var control7 = new google.visualization.ControlWrapper({
			        controlType: 'StringFilter',
			        containerId: 'control7',
			        options: {
			             filterColumnIndex: 6
			        }
			    });
			    
			    var control8 = new google.visualization.ControlWrapper({
			        controlType: 'StringFilter',
			        containerId: 'control8',
			        options: {
			             filterColumnIndex: 7
			        }
			    });
			    
			    var control9 = new google.visualization.ControlWrapper({
			        controlType: 'StringFilter',
			        containerId: 'control9',
			        options: {
			             filterColumnIndex: 8
			        }
			    });
			    
			    var control10 = new google.visualization.ControlWrapper({
			        controlType: 'StringFilter',
			        containerId: 'control10',
			        options: {
			             filterColumnIndex: 9
			        }
			    });
			    
			    var control11 = new google.visualization.ControlWrapper({
			        controlType: 'StringFilter',
			        containerId: 'control11',
			        options: {
			             filterColumnIndex: 10
			        }
			    });
			    
			    var control12 = new google.visualization.ControlWrapper({
			        controlType: 'StringFilter',
			        containerId: 'control12',
			        options: {
			             filterColumnIndex: 11
			        }
			    });
			    
			    var control13 = new google.visualization.ControlWrapper({
			        controlType: 'StringFilter',
			        containerId: 'control13',
			        options: {
			             filterColumnIndex: 12
			        }
			    });

			    dashboard.bind([control1, control2, control3, control4, control5, control6, control7, control8, control9, control10, control11, control12, control13], msgTable);
			    //dashboard.bind(control1, control2, msgTable);
			    dashboard.draw(data);

		 },
		 error: function(xhr){
			 alert("An error occured: " + xhr.status + " " + xhr.statusText);
		 }
	 });
	
  }

  function handleQueryResponse(response) {
    if (response.isError()) {
      alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage());
      return;
    }

    var data = response.getDataTable();
    var options = {
			 showRowNumber: true, 
			 width: '100%', 
			 height: '100%',
			 page: 'enable',
			 pageSize: 10
	 }
    
    var chart = new google.visualization.Table(document.getElementById('FleetTableInformation'));
    chart.draw(data, options);
  }