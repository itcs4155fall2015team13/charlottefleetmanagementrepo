/**
 *
*/

function login(){
	window.location.href = window.location.origin + "/Dashboard.jsp";
}

function parseLoginVerify(){
	username = $("#username").val();
	password = $("#password").val();
	Parse.initialize("mazjpfYKdEc7n7hOYffvCHJxO85vD47A6A133ZPy", "pxdM7U4u6kVauGpS74Qm03RcBNWDQdGJdtzlupPi");
	
	Parse.User.logIn(username, password, {
		  success: function(user) {
		    // Do stuff after successful login.			  
			  $.ajax({
					 type: "POST",    //define the type of ajax call (POST, GET, etc)
					 url: "FleetManagement",   //The name of the script you are calling
					 async: true,
					 data: {oper: "login", username: username, password: password},    //Your data you are sending to the script
					 success: function(data){
						 window.location.href = window.location.origin + data;
					 },
					 error: function(xhr){
						 alert("An error occured: " + xhr.status + " " + xhr.statusText);
					 }
				 });
		  },
		  error: function(user, error) {
		    // The login failed. Check error to see why.
			  ShowToast("Invalid Username/Password", 5, true);
		  }
	});		
}

function passwordReset(){
	
	Parse.initialize("mazjpfYKdEc7n7hOYffvCHJxO85vD47A6A133ZPy", "pxdM7U4u6kVauGpS74Qm03RcBNWDQdGJdtzlupPi");
	
	var email = $("#email").val();
	
	Parse.User.requestPasswordReset(email, {
		  success: function() {
		  // Password reset request was sent successfully
			alert("An email has been sent to " + email + " for resetting your password.");
			window.location.href = window.location.origin + "/CharlotteFleetManagement/Login.jsp";
		  },
		  error: function(error) {
		    // Show the error message somewhere
		    alert("Error: " + error.code + " " + error.message);
		  }
		});
}

function ShowToast(message, length, isError){
	 $('.Toast').stop();
	 
    if(isError == true){
           $('.Toast').css("background-color", "rgba(195, 4, 4, 0.9)");
    }else{
   	 $('.Toast').css("background-color", "rgba(8, 195, 4, 0.93)");
    }
    if(typeof length == 'undefined')
           length = 5;
   
    $('.Toast').text(message).fadeIn(400).delay(length * 1000).fadeOut(400);
}

function logoutUser(){
	
	Parse.initialize("mazjpfYKdEc7n7hOYffvCHJxO85vD47A6A133ZPy", "pxdM7U4u6kVauGpS74Qm03RcBNWDQdGJdtzlupPi");

	var currentUser = Parse.User.current();
	
	response.sendRedirect("Login.jsp");
	
	
}