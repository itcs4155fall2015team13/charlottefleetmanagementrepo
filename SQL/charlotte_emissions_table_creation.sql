USE [CharlotteFleetEmmissions]
GO

/****** Object:  Table [dbo].[vehicles]    Script Date: 12/17/2015 1:11:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[vehicles](
	[id] [float] NULL,
	[year] [float] NULL,
	[make] [nvarchar](255) NULL,
	[model] [nvarchar](255) NULL,
	[trany] [nvarchar](255) NULL,
	[cylinders] [float] NULL,
	[displ] [float] NULL,
	[tCharger] [nvarchar](255) NULL,
	[sCharger] [nvarchar](255) NULL,
	[city08] [float] NULL,
	[highway08] [float] NULL,
	[comb08] [float] NULL,
	[co2TailpipeGpm] [float] NULL,
	[ghgScore] [float] NULL,
	[feScore] [float] NULL,
	[fuelCost08] [float] NULL,	
	
) ON [PRIMARY]

GO


